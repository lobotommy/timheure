# TIMHEURE

Script jouant un son toutes les heures (xx:00). Demande de Actualia (https://www.youtube.com/channel/UC5M4fqmqLNJaBey0W_ylCSA)

## Installation

Lancer le script install.sh en superutilisateur

```bash
chmod +x install.sh
./install.sh
```
## Désinstallation

Lancer le script uninstall.sh en superutilisateur
```bash
chmod +x uninstall.sh
./uninstall.sh
```
## Usage
Une fois installé, rien à faire, on attend que ça bip

## License
Do What the fuck you want to Public License (ou payez moi un café à l'occase)