#!/bin/bash
########################################################################################
#
#Installation timheure
#
########################################################################################

 #Root check
if [[ $EUID -ne 0 ]]; then
   echo -e "\e[1mLance le en root, banane\e[0m" 
   exit 1
fi
#Installation
echo -e "\e[1mInstallation du script et du nécéssaire\e[0m"
if ! command -v mpg123 &> /dev/null
then
    echo -e "\e[1mmpg1233 n'est pas installé, t'inquiète mon grand, on gère\e[0m"
    apt install mpg123
fi
mkdir /usr/bin/timheure
mkdir /usr/share/timheure
cp timheure.sh /usr/bin/timheure
cp bip.mp3 /usr/share/timheure
chmod 755 /usr/bin/timheure/timheure.sh
chmod +x /usr/bin/timheure/timheure.sh
chmod 755 /usr/share/timheure
#Partie Systemd
echo -e "\e[1mInstallation et activation du service et autres trucs de geek\e[0m"
cp timheure.service /etc/systemd/system
cp timheure.timer /etc/systemd/system
systemctl daemon-reload
systemctl enable timheure.service
systemctl start timheure.service
systemctl enable timheure.timer
systemctl start timheure.timer 

echo -e "\e[1mInstallation terminée, a toi la gloire, les femmes et les voitures supersportives\e[0m"